package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;

import java.util.List;

public interface SetmealService extends IService<Setmeal> {

    //添加套餐 完成setmeal表和setmealDish的添加
    public void addmealWithmealDish(SetmealDto setmealDto);

    //根据id进行查询 用来回显修改套餐的信息
    public SetmealDto getByIdWithMealDish(Long id);

    //修改套餐和套餐关系表
    public void updateByIdWithMealDish(SetmealDto setmealDto);

    public void removeWithDish(List<Long> ids);
}
