package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.mapper.SetmealMapper;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class SetmealServiceImpl extends ServiceImpl<SetmealMapper, Setmeal> implements SetmealService {

    @Autowired
    private SetmealDishService setmealDishService;
    /**
     * 新增套餐 同时新增套餐和菜品的关系
     * @param setmealDto
     */
    @Override
    public void addmealWithmealDish(SetmealDto setmealDto) {

        this.save(setmealDto);

        //获取自动生成的套餐id
        Long setmealId = setmealDto.getId();

        //给套餐和菜品关系表添加id
        List<SetmealDish> setmealDish = setmealDto.getSetmealDishes();
        setmealDish = setmealDish.stream().map(item -> {
            item.setSetmealId(setmealId);
            return item;
        }).collect(Collectors.toList());

        //保存到关系表
        this.setmealDishService.saveBatch(setmealDish);
    }

    /**
     * 根据id进行查询套餐和菜品
     * @param id
     * @return
     */
    @Override
    public SetmealDto getByIdWithMealDish(Long id) {

        //先获取setmeal信息
        Setmeal setmeal = this.getById(id);
        //将基本信息拷贝给Dto
        SetmealDto setmealDto = new SetmealDto();
        BeanUtils.copyProperties(setmeal, setmealDto);

        //查询套餐对应的菜品
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> list = this.setmealDishService.list(queryWrapper);

        setmealDto.setSetmealDishes(list);

        return setmealDto;
    }

    /**
     * 修改套餐信息 和 对应的套餐菜品关系
     * @param setmealDto
     */
    @Override
    public void updateByIdWithMealDish(SetmealDto setmealDto) {

        //修改setmeal表
        this.updateById(setmealDto);
        //清理当前套餐的所有菜品
        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, setmealDto.getId());
        this.setmealDishService.remove(queryWrapper);

        List<SetmealDish> setmealDish = setmealDto.getSetmealDishes();
        //给菜品添加上setmealId 如果菜品变多 多出的部分不含有setmealId
        setmealDish = setmealDish.stream().map(item -> {
            item.setSetmealId(setmealDto.getId());
            return item;
        }).collect(Collectors.toList());

        //将数据插入数据库
        this.setmealDishService.saveBatch(setmealDish);
    }

    /**
     * 删除套餐，同时需要删除套餐和菜品的关联数据
     * @param ids
     */
    @Override
    public void removeWithDish(List<Long> ids) {
        //查询套餐状态 确定是否可以删除
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Setmeal::getId, ids);
        //状态1不能删除
        queryWrapper.eq(Setmeal::getStatus, 1);

        //查询是否含有记录
        int count = this.count(queryWrapper);
        if (count > 0) {
            throw  new CustomException("套餐正在售卖，不能删除");
        }
        //可以删除  先删除套餐表
        this.removeByIds(ids);
        //再删除关系表
        LambdaQueryWrapper<SetmealDish> queryWrapper1 = new LambdaQueryWrapper<>();
        queryWrapper1.in(SetmealDish::getSetmealId, ids);
        this.setmealDishService.remove(queryWrapper1);
    }
}
