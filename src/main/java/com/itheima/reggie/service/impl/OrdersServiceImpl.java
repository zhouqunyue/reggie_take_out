package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.CustomException;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.OrdersDto;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.mapper.OrdersMapper;
import com.itheima.reggie.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

    @Autowired
    private ShoppingCartService shoppingCartService;

    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private AddressBookService addressBookService;

    @Autowired
    private UserService userService;

    /**
     * 用户下单
     *
     * @param orders
     */
    @Override
    public void submit(Orders orders) {
        //获取当前用户id
        Long userId = BaseContext.getCurrentEmpId();
        orders.setUserId(userId);
        //获取用户数据
        User user = this.userService.getById(userId);
        //获取地址信息
        AddressBook addressBook = this.addressBookService.getById(orders.getAddressBookId());
        //查询当前用户购物车数据
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        List<ShoppingCart> list = this.shoppingCartService.list(queryWrapper);

        if (list == null || list.size() == 0) {
            throw new CustomException("购物车为空， 不能下单");
        }
        //订单明细表数据
        List<OrderDetail> orderDetails = new ArrayList<>();
        BigDecimal money = new BigDecimal(0);
        //生成订单号
        long orderId = IdWorker.getId();

        //统计金额
        for (ShoppingCart l : list) {
            BigDecimal amount = l.getAmount();
            BigDecimal number = BigDecimal.valueOf(l.getNumber());
            money = money.add(amount.multiply(number));

            OrderDetail orderDetail = new OrderDetail();
            BeanUtils.copyProperties(l, orderDetail);
            orderDetail.setOrderId(orderId);
            orderDetails.add(orderDetail);
        }
        orders.setAmount(money);
        //设置下单时间 等一系列信息
        orders.setNumber(String.valueOf(orderId));
        orders.setOrderTime(LocalDateTime.now());
        orders.setCheckoutTime(LocalDateTime.now());
        orders.setStatus(2);
        orders.setUserName(user.getName());
        orders.setConsignee(addressBook.getConsignee());
        orders.setPhone(addressBook.getPhone());
        orders.setAddress((addressBook.getProvinceName() == null ? "" : addressBook.getProvinceName())
                + (addressBook.getCityName() == null ? "" : addressBook.getCityName())
                + (addressBook.getDistrictName() == null ? "" : addressBook.getDistrictName())
                + (addressBook.getDetail() == null ? "" : addressBook.getDetail())
        );
        //向订单表插入数据 一条
        this.save(orders);
        //向订单明细表插入数据 多条
        this.orderDetailService.saveBatch(orderDetails);
        //最后清空购物车数据
        this.shoppingCartService.remove(queryWrapper);
    }



}
