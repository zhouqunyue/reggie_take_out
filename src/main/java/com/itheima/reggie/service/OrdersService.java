package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.OrdersDto;
import com.itheima.reggie.entity.Orders;

public interface OrdersService extends IService<Orders> {

    //完成用户下单操作
    public void submit(Orders orders);

    //管理端查看订单明细
}
