package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;

import java.util.List;

public interface DishService extends IService<Dish> {

    //新增菜品 同时插入菜品对应的口味数据  dish  dish_flavor
    public void saveWithFlavor(DishDto dishDto);

    //根据id查询菜品和对应的口味
    public DishDto getByIdWithFlavor(Long id);

    public void updateWithFlavor(DishDto dishDto);

    public void remove(List<Long> ids);
}
