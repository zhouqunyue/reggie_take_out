package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.DishFlavorService;
import com.itheima.reggie.service.DishService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * 菜品管理
 */
@RestController
@RequestMapping("/dish")
@Slf4j
public class DishController {

    @Autowired
    private DishService dishService;

    @Autowired
    private DishFlavorService dishFlavorService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 新增菜品
     *
     * @param dishDto
     * @return
     */
    @PostMapping
    public R<String> addFood(@RequestBody DishDto dishDto) {

        log.info("dishDto: {}", dishDto);

        this.dishService.saveWithFlavor(dishDto);

        return R.success("新增成功");
    }

    /**
     * 分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<Page> getByPage(int page, int pageSize, String name) {

        log.info("page = {}, pageSize = {}", page, pageSize);
        Page<Dish> pageInfo = new Page<>(page, pageSize);
        Page<DishDto> dtoInfo = new Page<>();

        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name), Dish::getName, name);
        queryWrapper.orderByDesc(Dish::getUpdateTime);

        this.dishService.page(pageInfo, queryWrapper);

        //进行对象拷贝  忽略records属性  缺少categoryName
        BeanUtils.copyProperties(pageInfo, dtoInfo, "records");

        //将List<Dish>处理成List<DishDto>
        List<Dish> records = pageInfo.getRecords();

        List<DishDto> list = records.stream().map(item -> {
            DishDto dishDto = new DishDto();
            //将其他属性拷贝给disDto
            BeanUtils.copyProperties(item, dishDto);
            //获取分类id
            Long categoryId = item.getCategoryId();
            //查询分类表 获取名字
            Category category = this.categoryService.getById(categoryId);

            if (category != null) {
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }

            return dishDto;
        }).collect(Collectors.toList());

        //赋值给dto
        dtoInfo.setRecords(list);

        return R.success(dtoInfo);
    }

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<DishDto> getById(@PathVariable Long id) {
        log.info("根据id查询, id: {}", id);

        DishDto dishDto = this.dishService.getByIdWithFlavor(id);
        return R.success(dishDto);
    }

    /**
     * 修改菜品
     *
     * @param dishDto
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody DishDto dishDto) {

        log.info("修改信息: {}", dishDto);

        this.dishService.updateWithFlavor(dishDto);

        //清理某个分类下面菜品缓存的数据
        String key = "dish_" + dishDto.getCategoryId() + "_1";
        //删除对应的缓存数据
        this.redisTemplate.delete(key);
        return R.success("修改成功");
    }

    /**
     * 批量停售或者起售
     *
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    public R<String> updateStatus(@PathVariable Integer status, Long[] ids) {
        log.info("修改状态: status = {}, ids = {}", status, Arrays.toString(ids));

        List<Dish> list = new ArrayList<>();

        for (Long id : ids) {
            Dish dish = this.dishService.getById(id);
            dish.setStatus(status);
//            this.dishService.updateById(dish);
            list.add(dish);
        }

        this.dishService.updateBatchById(list);
        return R.success("修改成功");
    }

    /**
     * 批量或单个删除
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> deleteByIds(@RequestParam List<Long> ids) {

        log.info("ids = {}", ids);

        this.dishService.remove(ids);
        return R.success("删除成功");
    }

    /**
     * 获取所有菜品
     *
     * @param dish
     * @return
     */
    @GetMapping("/list")
    public R<List<DishDto>> getDish(Dish dish) {

        List<DishDto> dishDtoList = null;
        //拼接对应的key
        String key = "dish_" + dish.getCategoryId() + "_" + dish.getStatus();
        //先从redis中获取缓存数据
        dishDtoList = (List<DishDto>) this.redisTemplate.opsForValue().get(key);
        //如果存在 直接返回 无需查询数据库
        if (dishDtoList != null) {
            return R.success(dishDtoList);
        }

        //不存在 查询数据库 将查询到的菜品数据缓存到redis


        Long categoryId = dish.getCategoryId();
        log.info("菜品: {}", categoryId);

        LambdaQueryWrapper<Dish> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(Dish::getCategoryId, categoryId).eq(Dish::getStatus, 1);
        //排序
        queryWrapper.orderByAsc(Dish::getSort).orderByDesc(Dish::getUpdateTime);

        List<Dish> list = this.dishService.list(queryWrapper);

        dishDtoList = list.stream().map(item -> {
            DishDto dishDto = new DishDto();
            BeanUtils.copyProperties(item, dishDto);

            //获取分类id
            Long categoryId1 = item.getCategoryId();
            //查询分类表 获取名字
            Category category = this.categoryService.getById(categoryId1);

            if (category != null) {
                String categoryName = category.getName();
                dishDto.setCategoryName(categoryName);
            }
            //查询口味表
            LambdaQueryWrapper<DishFlavor> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.eq(DishFlavor::getDishId, dishDto.getId());
            List<DishFlavor> list1 = this.dishFlavorService.list(queryWrapper1);

            dishDto.setFlavors(list1);
            return dishDto;
        }).collect(Collectors.toList());

        //将数据存进redis缓存  时间60分钟
        this.redisTemplate.opsForValue().set(key, dishDtoList, 60, TimeUnit.MINUTES);


        return R.success(dishDtoList);

    }
}
