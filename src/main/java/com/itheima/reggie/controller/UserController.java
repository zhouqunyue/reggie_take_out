package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.service.UserService;
import com.itheima.reggie.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 发送手机短信验证码
     *
     * @param user
     * @return
     */
    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session) {
        //获取手机号
        String phone = user.getPhone();
        if (StringUtils.isNotEmpty(phone)) {
            //生成验证码
            String code = ValidateCodeUtils.generateValidateCode(4).toString();
            log.info("code = {}", code);
            //调用阿里云提供的短信服务发送短信
            //......
            //需要将生成的验证码保存到Session
//            session.setAttribute(phone, code);

            //将生成的验证码缓存到redis中，并且设置有效期为5分钟
            this.redisTemplate.opsForValue().set(phone, code, 5, TimeUnit.MINUTES);

            return R.success("手机验证码短信发送成功");
        }
        return R.error("短信发送失败");
    }

    /**
     * 移动端用户登录
     *
     * @param user
     * @param session
     * @return
     */
    @PostMapping("/login")
    public R<User> userLogin(@RequestBody Map user, HttpSession session) {

        log.info(user.toString());

        //获取手机号
        String phone = user.get("phone").toString();
        //获取验证码
        String code = user.get("code").toString();
        //进行验证码比对
//        String rightCode = (String) session.getAttribute(phone);

        //从redis中获取验证码
        Object rightCode = this.redisTemplate.opsForValue().get(phone);
        //比对无误 登录成功
        if (rightCode != null && rightCode.equals(code)) {
            //判断当前用户是否是新用户 帮助自动完成注册
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getPhone, phone);

            User user1 = this.userService.getOne(queryWrapper);
            if (user1 == null) {
                //新用户 完成注册
                user1 = new User();
                user1.setPhone(phone);
                user1.setStatus(1);
                this.userService.save(user1);
            }
            //设置session
            session.setAttribute("user", user1.getId());

            //用户登录成功 删除redis中缓存的验证码
            this.redisTemplate.delete(phone);

            return R.success(user1);

        }

        return R.error("登录失败");
    }

    @PostMapping("/loginout")
    public R<String> userLogout(HttpSession session) {

        log.info("登出功能");
        session.removeAttribute("user");
        return R.success("退出成功");
    }
}
