package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/shoppingCart")
@Slf4j
public class ShoppingCartController {

    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加套餐或菜品
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    public R<ShoppingCart> addShoppingCart(@RequestBody ShoppingCart shoppingCart) {

        log.info("添加购物车: {}", shoppingCart);

        //设置创建时间
        Long userId = BaseContext.getCurrentEmpId();
        shoppingCart.setCreateTime(LocalDateTime.now());
        //设置userId
        shoppingCart.setUserId(userId);

        Long dishId = shoppingCart.getDishId();
        //判断加进购物车的是菜品还是套餐
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);

        if (dishId != null) {
            //菜品
            queryWrapper.eq(ShoppingCart::getDishId, dishId);
        } else {
            //套餐
            queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }

        //查询当前菜品或者套餐是否在购物车中
        ShoppingCart shoppingCartServiceOne = this.shoppingCartService.getOne(queryWrapper);
        if (shoppingCartServiceOne != null) {
            //存在  在原来基础上+1
            shoppingCartServiceOne.setNumber(shoppingCartServiceOne.getNumber() + 1);
            this.shoppingCartService.updateById(shoppingCartServiceOne);
        } else {
            //新增数据
            shoppingCart.setNumber(1);
            this.shoppingCartService.save(shoppingCart);
            shoppingCartServiceOne = shoppingCart;
        }

        return R.success(shoppingCartServiceOne);
    }

    /**
     * 获取购物车信息
     * @return
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> getList() {

        log.info("购物车");

        //获取当前用户id
        Long userId = BaseContext.getCurrentEmpId();
        //根据id把购物车的东西查询出来
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);

        List<ShoppingCart> list = this.shoppingCartService.list(queryWrapper);

        return R.success(list);
    }

    /**
     * 减少菜品或者套餐
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    public R<ShoppingCart> subShoppingCart(@RequestBody ShoppingCart shoppingCart) {

        log.info("减少菜品、套餐: {}", shoppingCart.toString());

        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        //设置userId
        Long userId = BaseContext.getCurrentEmpId();
        shoppingCart.setUserId(userId);

        //首先判断菜品or套餐
        if (shoppingCart.getDishId() != null) {
            //菜品
            queryWrapper.eq(ShoppingCart::getDishId, shoppingCart.getDishId());
        } else {
            //套餐
            queryWrapper.eq(ShoppingCart::getSetmealId, shoppingCart.getSetmealId());
        }
        ShoppingCart sc = this.shoppingCartService.getOne(queryWrapper);
        //大于1 -1
        if (sc.getNumber() > 1) {
            sc.setNumber(sc.getNumber() - 1);
            this.shoppingCartService.updateById(sc);
        } else {
            //等于1  删除记录
            sc.setNumber(0);
            this.shoppingCartService.removeById(sc.getId());
        }

        return R.success(sc);
    }

    @DeleteMapping("/clean")
    public R<String> cleanShoppingCart() {

        log.info("清空购物车");

        //获取userId
        Long userId = BaseContext.getCurrentEmpId();
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        this.shoppingCartService.remove(queryWrapper);
        return R.success("操作成功");
    }
}
