package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.AddressBook;
import com.itheima.reggie.service.AddressBookService;
import com.itheima.reggie.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/addressBook")
@Slf4j
public class AddressBookController {

    @Autowired
    private AddressBookService addressBookService;

    @Autowired
    private UserService userService;

    /**
     * 新增收获地址
     * @param addressBook
     * @return
     */
    @PostMapping
    public R<String> saveAddress(@RequestBody AddressBook addressBook) {

        log.info("新增地址: {}", addressBook);
        //获取userId
        Long userId = BaseContext.getCurrentEmpId();
        addressBook.setUserId(userId);

        this.addressBookService.save(addressBook);
        return R.success("保存成功");
    }

    /**
     * 获取所有收货地址
     * @return
     */
    @GetMapping("/list")
    public R<List<AddressBook>> getAddress() {

        log.info("查询地址");
        //获取userId
        Long userId = BaseContext.getCurrentEmpId();

        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, userId);
        List<AddressBook> list = this.addressBookService.list(queryWrapper);

        return R.success(list);
    }

    /**
     * 设置默认地址
     * @param addressBook
     * @return
     */
    @PutMapping("/default")
    public R<String> setDefaultAdd(@RequestBody AddressBook addressBook) {

        log.info("设置默认地址: {}", addressBook);

        //修改其他默认地址为非默认地址
        LambdaUpdateWrapper<AddressBook> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(AddressBook::getUserId, BaseContext.getCurrentEmpId()).eq(AddressBook::getIsDefault, 1);
        updateWrapper.set(AddressBook::getIsDefault, 0);
        this.addressBookService.update(updateWrapper);

        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getId, addressBook.getId());
        addressBook.setIsDefault(1);
        this.addressBookService.updateById(addressBook);

        return R.success("修改成功");
    }

    /**
     * 根据id查询 回显数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<AddressBook> getById(@PathVariable Long id) {

        log.info("根据id查询: {}", id);
        AddressBook addressBook = this.addressBookService.getById(id);

        return R.success(addressBook);
    }

    /**
     * 修改地址
     * @param addressBook
     * @return
     */
    @PutMapping
    public R<String> updateAddress(@RequestBody AddressBook addressBook) {

        log.info("修改地址: {}", addressBook);
        this.addressBookService.updateById(addressBook);

        return R.success("修改成功");
    }

    /**
     * 删除地址
     * @param ids
     * @return
     */
    @DeleteMapping
    public R<String> delAddress(Long ids) {

        log.info("删除地址: {}", ids);

        this.addressBookService.removeById(ids);
        return R.success("删除成功");
    }

    /**
     * 获取默认地址
     * @return
     */
    @GetMapping("/default")
    public R<AddressBook> getDefault() {

        log.info("获取默认地址");

        //获取userId
        Long userId = BaseContext.getCurrentEmpId();
        //设置查询条件
        LambdaQueryWrapper<AddressBook> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(AddressBook::getUserId, userId);
        //查询默认地址条件
        queryWrapper.eq(AddressBook::getIsDefault, 1);

        AddressBook addressBook = this.addressBookService.getOne(queryWrapper);
        return R.success(addressBook);
    }

}
