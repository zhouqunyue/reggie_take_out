package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.entity.SetmealDish;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 套餐管理
 */
@RestController
@RequestMapping("/setmeal")
@Slf4j
public class SetmealController {

    @Autowired
    private SetmealService setmealService;

    @Autowired
    private SetmealDishService setmealDishService;

    @Autowired
    private CategoryService categoryService;

    /**
     * 新增套餐
     *
     * @param setmealDto
     * @return
     */
    @CacheEvict(value = "setmealCache", allEntries = true)
    @PostMapping
    public R<String> addSetMeal(@RequestBody SetmealDto setmealDto) {

        log.info("新增套餐: {}", setmealDto);

        this.setmealService.addmealWithmealDish(setmealDto);
        return R.success("添加成功");
    }

    /**
     * 分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    public R<Page> getByPage(int page, int pageSize, String name) {

        log.info("page = {} pageSize = {} name = {}", page, pageSize, name);

        //套餐分页查询
        Page<Setmeal> pageOfSetmeal = new Page<>(page, pageSize);

        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(name != null, Setmeal::getName, name);
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);
        //查询套餐基本信息
        this.setmealService.page(pageOfSetmeal, queryWrapper);

        //SetmealDto
        Page<SetmealDto> pageInfo = new Page<>();

        //将基本信息拷贝给Dto
        BeanUtils.copyProperties(pageOfSetmeal, pageInfo, "records");

        //对records进行处理
        List<Setmeal> records = pageOfSetmeal.getRecords();

        List<SetmealDto> list = records.stream().map(item -> {
            SetmealDto setmealDto = new SetmealDto();
            //将基本属性拷贝给Dto
            BeanUtils.copyProperties(item, setmealDto);
            //获取分类id
            Long categoryId = item.getCategoryId();
            //查询对应的信息
            Category category = this.categoryService.getById(categoryId);
            //设置套餐名字
            if (category != null) {
                setmealDto.setCategoryName(category.getName());
            }

            return setmealDto;
        }).collect(Collectors.toList());

        pageInfo.setRecords(list);

        return R.success(pageInfo);
    }

    /**
     * 根据id查询 （修改时回显信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Setmeal> getById(@PathVariable Long id) {

        log.info("id", id);

        SetmealDto setmealDto = this.setmealService.getByIdWithMealDish(id);
        return R.success(setmealDto);
    }

    /**
     * 修改信息
     * @param setmealDto
     * @return
     */
    @PutMapping
    public R<String> update(@RequestBody SetmealDto setmealDto) {

        log.info("修改信息: {}", setmealDto.toString());

        this.setmealService.updateByIdWithMealDish(setmealDto);
        return R.success("修改成功");
    }

    /**
     * 删除操作
     * @param ids
     * @return
     */
    @CacheEvict(value = "setmealCache", allEntries = true)
    @DeleteMapping
    public R<String> deleteByIds(@RequestParam List<Long> ids) { //@RequestParam  用于传递可变参数
        log.info("删除套餐ids: {}", ids);

        this.setmealService.removeWithDish(ids);
        return R.success("删除成功");
    }

    @PostMapping("/status/{status}")
    public R<String> updateStatus(@PathVariable int status, Long[] ids) {

        log.info("status = {}, ids = {}", status, Arrays.toString(ids));

        List<Setmeal> list = new ArrayList<>();

        for (Long id : ids) {
            Setmeal setmeal = this.setmealService.getById(id);
            setmeal.setStatus(status);
            list.add(setmeal);
//            this.setmealService.updateById(setmeal);
        }

        this.setmealService.updateBatchById(list);


        return R.success("操作成功");
    }

    /**
     * 根据条件查询套餐数据
     * @param setmeal
     * @return
     */
    @Cacheable(value = "setmealCache", key = "#setmeal.categoryId + '_' + #setmeal.status")
    @GetMapping("/list")
    public R<List<Setmeal>> getSetmeal(Setmeal setmeal) {

        log.info("套餐: {}", setmeal);

        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Setmeal::getCategoryId, setmeal.getCategoryId());
        queryWrapper.eq(Setmeal::getStatus, setmeal.getStatus());
        queryWrapper.orderByDesc(Setmeal::getUpdateTime);

        List<Setmeal> list = this.setmealService.list(queryWrapper);

        return R.success(list);
    }
}
