package com.itheima.reggie.controller;

import com.itheima.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.UUID;

/**
 * 文件上传和下载
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class CommonController {


    //转存文件的路劲
    @Value("${reggie.path}")
    private String basePath;
    /**
     * 文件上传
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public R<String> upload(MultipartFile file) {
        //file临时文件 需要转存到指定位置
        log.info(file.toString());

        String oriName = file.getOriginalFilename();

        //截取原始文件后缀
        String suffix = oriName.substring(oriName.lastIndexOf("."));
        //使用uuid重新生成文件名， 防止文件名重复造成的覆盖
        String fileName = UUID.randomUUID().toString() + suffix;

        //创建一个目录对象
        File dir = new File(this.basePath);

        //判断目录是否存在
        if (!dir.exists()) {
            //不存在 创建
            dir.mkdirs();
        }
        try {
            //将临时文件转存到指定位置
            file.transferTo(new File(this.basePath + fileName));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        //将文件名字响应回去  方便读取文件
        return R.success(fileName);
    }

    /**
     * 文件下载
     * @param name
     * @param response
     */
    @GetMapping("/download")
    public void download(String name, HttpServletResponse response) {
        //输入流 通过输入流读取文件内容
        try {
            FileInputStream fs = new FileInputStream(new File(this.basePath + name));

            //输出流 通过输出流将文件写到浏览器
            ServletOutputStream os = response.getOutputStream();

            //设置ContentType
            response.setContentType("image/jpeg");
            byte[] bytes = new byte[1024];
            int len = 0;

            while ((len = fs.read(bytes)) != -1) {
                os.write(bytes, 0, len);

                os.flush();
            }

            os.close();
            fs.close();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
