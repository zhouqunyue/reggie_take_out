package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.OrdersDto;
import com.itheima.reggie.entity.OrderDetail;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.service.OrderDetailService;
import com.itheima.reggie.service.OrdersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @Autowired
    private OrderDetailService orderDetailService;

    /**
     * 支付操作
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders) {

        log.info("支付订单: {}", orders.toString());

        this.ordersService.submit(orders);
        return R.success("操作成功");
    }

    /**
     * 查询订单
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    public R<Page> getByPage(int page, int pageSize) {

        log.info("page = {} pageSize = {}", page, pageSize);
        //获取userId
        Long userId = BaseContext.getCurrentEmpId();
        Page<Orders> pageInfo = new Page<>(1, 1);
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();

        queryWrapper.eq(Orders::getUserId, userId);
        this.ordersService.page(pageInfo, queryWrapper);
        return R.success(pageInfo);
    }

    @GetMapping("/page")
    public R<Page> getByPage(int page, int pageSize,String number, String beginTime, String endTime) {

        log.info("page = {} pageSize = {}, number = {} beginTime = {}, endTime = {}", page, pageSize, number, beginTime, endTime);


        Page<Orders> ordersPage = new Page<>(page, pageSize);
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(number != null, Orders::getNumber, number);

        if (beginTime != null && endTime != null) {
            queryWrapper.ge(Orders::getOrderTime, LocalDateTime.parse(beginTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
            queryWrapper.lt(Orders::getOrderTime, LocalDateTime.parse(endTime, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));

        }


        //查询符合条件的orders
        this.ordersService.page(ordersPage, queryWrapper);

        Page<OrdersDto> DtoPage = new Page<>();

        BeanUtils.copyProperties(ordersPage, DtoPage, "records");
        List<Orders> records = ordersPage.getRecords();

        List<OrdersDto> records1 = records.stream().map(item -> {

            OrdersDto ordersDto = new OrdersDto();
            //拷贝基本属性
            BeanUtils.copyProperties(item, ordersDto);
            //获取订单号
            String orderId = item.getNumber();
            LambdaQueryWrapper<OrderDetail> queryWrapper1 = new LambdaQueryWrapper<>();
            queryWrapper1.eq(OrderDetail::getOrderId, orderId);
            //从订单明细表中获取数据
            List<OrderDetail> list = this.orderDetailService.list(queryWrapper1);
            ordersDto.setOrderDetails(list);

            return ordersDto;
        }).collect(Collectors.toList());

        DtoPage.setRecords(records1);

        return R.success(DtoPage);
    }
}
