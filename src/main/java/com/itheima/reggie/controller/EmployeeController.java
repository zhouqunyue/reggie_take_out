package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    /**
     * 员工登录
     *
     * @param request  获取当前用户 设置session
     * @param employee
     * @return
     */
    @PostMapping("/login")
    public R<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {

        //将页面提交的密码进行md5加密
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        //根据页面提交的用户名username查询数据库
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Employee::getUsername, employee.getUsername());
        Employee emp = this.employeeService.getOne(queryWrapper);

        //如果没有查询到则返回登陆失败的结果
        if (emp == null) {
            return R.error("用户不存在");
        }

        //用户存在  密码比对，如果不一样则返回登陆失败结果
        if (!emp.getPassword().equals(password)) {
            return R.error("密码错误");
        }

        //查看员工状态 如果为禁用状态 则返回员工已禁用结果
        if (!(emp.getStatus() == 1)) {
            return R.error("员工状态已禁用");
        }

        //登录成功
        request.getSession().setAttribute("employee", emp.getId());

        return R.success(emp);
    }

    /**
     * 员工退出
     *
     * @param request
     * @return
     */
    @PostMapping("/logout")
    public R<String> loginOut(HttpServletRequest request) {
        //清理session保存的登录员工id
        request.getSession().removeAttribute("employee");
        return R.success("退出成功");
    }

    /**
     * 新增员工
     *
     * @param employee
     * @param request
     * @return
     */
    @PostMapping
    public R<String> addEmployee(HttpServletRequest request, @RequestBody Employee employee) {
        log.info("新增员工，员工信息: {}", employee.toString());

        //设置初始密码 需要进行md5加密
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));

        //使用MP公共字段自动填充
        //设置创建时间
        //employee.setCreateTime(LocalDateTime.now());
        //设置更新时间
        //employee.setUpdateTime(LocalDateTime.now());

        //设置创建人id
        //employee.setCreateUser((Long) request.getSession().getAttribute("employee"));
        //设置更新人
        //employee.setUpdateUser((Long) request.getSession().getAttribute("employee"));

        //将数据入库
        this.employeeService.save(employee);
        return R.success("添加成功");
    }

    /**
     * 分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping({"/page"})
    public R<Page> getByPage(int page, int pageSize, String name) {

        log.info("page = {}, pageSize = {}, name = {}", page, pageSize, name);
        //分页构造器
        Page<Employee> pageInfo = new Page<>(page, pageSize);
        //条件构造器
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(name), Employee::getName, name).orderByDesc(Employee::getUpdateTime);
        //执行查询
        this.employeeService.page(pageInfo, queryWrapper);

        return R.success(pageInfo);
    }

    /**
     * 根据id修改员工信息
     * @param employee
     * @return
     */
    @PutMapping
    public R<String> update(HttpServletRequest request, @RequestBody Employee employee) {

        log.info(employee.toString());

        //获取线程id
        long id = Thread.currentThread().getId();
        log.info("线程id为: {}", id);

        //Long empId = (Long) request.getSession().getAttribute("employee");
        //修改更新人
        //employee.setUpdateUser(empId);
        //修改更新时间
        //employee.setUpdateTime(LocalDateTime.now());
        //执行修改操作
        this.employeeService.updateById(employee);
        return R.success("修改成功");
    }

    /**
     * 按照id查询数据 回显到修改界面
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public R<Employee> getById(@PathVariable Long id) {
        log.info("按照id查询");

        Employee emp = this.employeeService.getById(id);
        if (emp != null) {
            return R.success(emp);
        }

        return R.error("用户不存在");
    }
}
