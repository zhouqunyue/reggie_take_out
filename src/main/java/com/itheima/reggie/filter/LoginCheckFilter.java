package com.itheima.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 检查用户是否已经完成了登录
 */
@WebFilter(filterName = "loginCheckFilter", urlPatterns = "/*")  //拦截器名称 和 拦截的请求类型（所有
@Slf4j
public class LoginCheckFilter implements Filter {

    //路径匹配器  支持通配符
    public static final AntPathMatcher PATH_MATCHER = new AntPathMatcher();

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        //1 获取本次请求的URI
        String URI = request.getRequestURI();

        log.info("拦截到请求: {}", URI);

        //定义不需要拦截的资源
        String[] urls = new String[]{
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/user/sendMsg",//移动端发送短信
                "/user/login"//移动端登录
        };

        //2 判断本次请求是否需求处理
        boolean check = this.check(urls, URI);
        //3 如果不需要处理 直接放行
        if (check) {
            log.info("本次请求{}不需要处理", URI);
            filterChain.doFilter(request, response);
            return;
        }

        //4-1 判断登录状态 如果已经登录 直接放行  (员工后台
        Long emp = (Long) request.getSession().getAttribute("employee");
        if (emp != null) {
            log.info("用户已登录, id{}", request.getSession().getAttribute("employee"));

            //把用户id存到BaseContext中
            BaseContext.setCurrentEmpId(emp);

            //放行
            filterChain.doFilter(request, response);
            return;
        }

        //4-2 判断登录状态 如果已经登录 直接放行(移动端
        Long user = (Long) request.getSession().getAttribute("user");
        if (user != null) {
            log.info("用户已登录, id{}", request.getSession().getAttribute("user"));

            //把用户id存到BaseContext中
            BaseContext.setCurrentEmpId(user);

            //放行
            filterChain.doFilter(request, response);
            return;
        }

        log.info("用户未登录");
        //如果未登录 则返回未登录结果，通过输出流方式向客户端页面响应数据      NOTLOGIN和前端约定好的协议 如果返回这个 跳转到登录
        response.getWriter().write(JSON.toJSONString(R.error("NOTLOGIN")));

        return;
    }

    /**
     * 路径匹配  检查本次请求是否需要放行
     * @param requestURI
     * @param urls
     * @return
     */
    public boolean check(String[] urls, String requestURI) {
        for (String url : urls) {
            boolean match = PATH_MATCHER.match(url, requestURI);
            if (match) {
                //不要拦截的内容
                return true;
            }
        }
        //需要拦截的内容
        return false;
    }
}

