package com.itheima.reggie.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * 全局异常处理
 */
//拦截指定的控制器
@ControllerAdvice(annotations = {RestController.class, Controller.class})
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

    /**
     * 异常处理方法
     *
     * @return
     */
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R<String> exceptionHandler(SQLIntegrityConstraintViolationException ex) {

        log.error(ex.getMessage());

        if (ex.getMessage().contains("Duplicate entry")) {
            //确定是账号重复错误
            String[] split = ex.getMessage().split(" ");
            String msg = split[2] + "已存在";
            log.error(msg);

            return R.error(msg + "已存在");
        }
        return R.error("未知错误");
    }

    /**
     * 删除菜品异常
     *
     * @return
     */
    @ExceptionHandler(CustomException.class)
    public R<String> exceptionHandler(CustomException ex) {

        log.error(ex.getMessage());

        return R.error(ex.getMessage());
    }

}
