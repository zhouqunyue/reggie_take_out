package com.itheima.reggie.common;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 自定义元数据处理器
 */
@Component
@Slf4j
public class MyMetaObjectHandler implements MetaObjectHandler {


    /**
     * 插入操作自动填充
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        log.info("公共字段自动填充[insert]...");
        log.info(metaObject.toString());

        metaObject.setValue("createTime", LocalDateTime.now());
        metaObject.setValue("updateTime", LocalDateTime.now());

        //获取用户id
        Long empId = BaseContext.getCurrentEmpId();

        metaObject.setValue("createUser", empId);
        metaObject.setValue("updateUser", empId);

    }

    /**
     * 更新是自动填充
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        log.info("公共字段自动填充[update]...");

        log.info(metaObject.toString());

        long id = Thread.currentThread().getId();
        log.info("线程id为: {}", id);


        metaObject.setValue("updateTime", LocalDateTime.now());

        //获取用户id
        Long empId = BaseContext.getCurrentEmpId();

        metaObject.setValue("updateUser", empId);
    }
}
