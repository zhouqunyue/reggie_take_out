package com.itheima.reggie.common;

/**
 * 基于ThreadLocal封装工具类，用户保存和获取当前登录用户id
 */
public class BaseContext {

    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 设置对应的用户id
     * @param id
     */
    public static void setCurrentEmpId(Long id) {
        threadLocal.set(id);
    }

    /**
     * 获取对应的用户id
     * @return
     */
    public static Long getCurrentEmpId() {
        return threadLocal.get();
    }
}

/*
ThreadLocal并不是一个Thread,而是Thread的一个局部变量，该变量的线程提供独立的变量副本，所有每一个线程都可以独立的改变自己的副本，
不会影响到其他副本，每个线程提供一份存储空间，具有线程隔离效果

常用方法：
    set      设置当前线程的线程变量的值
    get         返回当前线程所对应的线程局部变量的值

    解决获取用户id的问题
 */